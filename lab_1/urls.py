from django.urls import path
from .views import index, home, project, profile, interest
from story5.views import addMatkul, viewListMatkul, viewSpecificMatkul, deleteMatkul

#urls for app
urlpatterns = [
    path('story_1/', index, name='index'),
    path('', home, name='home'),
    path('project/', project, name='project'),
    path('profile/', profile, name='profile'),
    path('interest/', interest, name='interest'),
    path('add-matakuliah/', addMatkul, name='add_matkul'),
    path('matakuliah/', viewListMatkul, name='list_matkul'),
    path('matakuliah/<int:id>', viewSpecificMatkul, name='specified_matkul'),
    path('matakuliah/<int:id>/del', deleteMatkul, name='delete_matkul'),

]
