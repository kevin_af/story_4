from django.shortcuts import render
from datetime import datetime, date
# Enter your name here
mhs_name = 'Kevina Felie Arihadi'
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(2002, 4, 5)
npm = 1906298973 
major = 'Information System'
uni = 'University of Indonesia'
angkatan = 'Maung 2019'
birthday = birth_date.strftime("%d %B %Y")


# Create your views here.
def index(request):
    response = {'name': mhs_name, 'age': calculate_age(birth_date.year), 'npm': npm, 'major': major, 'uni': uni, 'angkatan': angkatan, 'title': "Story 1"}
    return render(request, 'index_lab1.html', response)

def home(request):
    response = {'title': "Home"}
    return render(request, 'home.html', response)

def project(request):
    response = {'title': "Project"}
    return render(request, 'project.html', response)

def profile(request): 
    response = {'name': mhs_name, 'age': calculate_age(birth_date.year), 'npm': npm, 'major': major, 'uni': uni, 'angkatan': angkatan, 'birthday': birthday, 'title': "Profile"}
    return render(request, 'profile.html', response)

def interest(request):
    response = {'title': "Interest"}
    return render(request, 'interest.html', response)

def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0
