from django.db.models import fields
from django.forms import ModelForm
from .models import MatKul

class FormMatkul(ModelForm):
    class Meta:  #override meta
        model = MatKul
        fields = ['name','sks','dosen','classes','semester','desc']