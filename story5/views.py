from django.shortcuts import render, redirect
from .forms import FormMatkul
from .models import MatKul

# Create your views here.
def addMatkul(request):
    form = FormMatkul(request.POST or None)
    if (form.is_valid() and request.method == 'POST'):
        form.save()
        return redirect('list_matkul')
    context = {'form' : form}       #is context and response the same?
    return render(request, 'add_matkul.html', context)

def viewListMatkul(request):
    mataKuliah = MatKul.objects.all()
    context = {'mataKuliah' : mataKuliah}
    return render(request, 'list_matkul.html', context)

def viewSpecificMatkul(request, id):
    mataKuliah = MatKul.objects.get(id = id)
    context = {'mataKuliah' : mataKuliah, 'title' : mataKuliah.nama}
    return render(request, 'specified_matkul.html', context)

def deleteMatkul(request, id):
    mataKuliah = MatKul.objects.get(id = id)
    mataKuliah.delete()
    return redirect('list_matkul')