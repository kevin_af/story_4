from django.db import models

# Create your models here.
class MatKul(models.Model):
    name = models.CharField(max_length=150)
    sks = models.PositiveSmallIntegerField()
    dosen = models.CharField(max_length=150)
    classes = models.CharField(max_length=100)
    semester = models.CharField(max_length=50)
    desc = models.TextField()

    def __str__(self):
        return f'{self.name}'
